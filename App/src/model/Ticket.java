package model;


import java.time.LocalDateTime;

public class Ticket {
    public int id;
    public String from;
    public String to;
    public int seatNumber;
    public LocalDateTime flightDate;
    public int seatCount = 50;

    public Ticket(int id, String from, String to, int seatNumber, LocalDateTime flightDate) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.seatNumber = seatNumber;
        this.flightDate = flightDate;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", seatNumber=" + seatNumber +
                ", flightDate=" + flightDate +
                ", seatCount=" + seatCount +
                '}';
    }
}
