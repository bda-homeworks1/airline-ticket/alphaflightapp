package model;

import java.util.ArrayList;

public class User {
   public int id;
    public  String username;
    public   String password;
    public ArrayList<Ticket> boughetfligths;
    public ArrayList<String> savedfligths;
    public String phoneNumber;
    public  Card card;

    public User(int id, String username, String password, String phoneNumber, Card card) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.card = card;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", boughetfligths=" + boughetfligths +
                ", savedfligths=" + savedfligths +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", card=" + card +
                '}';
    }
}
