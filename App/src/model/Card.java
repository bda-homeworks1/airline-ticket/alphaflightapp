package model;

public class Card {
        String ownerOfCard;
        String cardNumber;
        String password;

    public Card(String ownerOfCard, String cardNumber, String password) {
        this.ownerOfCard = ownerOfCard;
        this.cardNumber = cardNumber;
        this.password = password;
    }

    @Override
    public String toString() {
        return
                "cardNumber='" + cardNumber + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
